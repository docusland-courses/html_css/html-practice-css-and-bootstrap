
# Entraînement HTML - CSS - Bootstrap
#HTML #CSS #Bootstrap
> Projet à destination d'étudiants souhaitant pratiquer et approfondir les bases de l'intégration web.

Bonjour, voici un exercice vous permettant de vous entrainer en html et en css. Vous pouvez bien sûr utiliser [Bootstrap](https://getbootstrap.com/).

Pour la petite histoire : c'est un thème créé pour les débutants de Bootstrap, et vu que vous êtes en train d'apprendre à développer en html, css... Eh bien, vous allez coder ce thème au lieu de l'utiliser! 😄

Implémentez le site maquetté au sein du dossier example. Et votre site doit ressembler à maquette_responsive.png une fois qu'on réduit la fenêtre. Pensez au mobile first lorsque vous ecrivez le css.


## Ce qui ne peut être représenté au sein des maquettes :

* La navbar est fixe et suis la navigation. De plus elle est transparente quand on arrive sur la page et devient blanche quand on commence à scroller.

* N'oubliez pas les hovers sur les call-to-actions, les liens sur les boutons etc... De plus il y a un hover sur les images, comme montré dans l'image hover_exemple.png

* Il y a une animation sur les icones de la section "at your service"; elles apparaissent en bounceIn très rapide.



## Ressources :

Les images sont disponibles dans le dossier images.

La couleur orange est le : #f05f40 .

Les liens pour la police :

- \<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css"\>

- \<link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css"\>

Les icones sont générées via font-awesome mais vous pouvez aussi utiliser celles disponibles dans le dossier images .

Bon courage!
